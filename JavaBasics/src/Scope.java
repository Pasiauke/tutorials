/**
 * @author Paweł Kołczyk-Kasprzycki pawel.kolczyk-kasprzycki@gmail.com
 * <p>
 * This purpose of this class is show the scope of variables in Java.
 */

public class Scope {

    private static final String classScopeVariable = "This variable's scope is class";

    public static void main(String... args) {

        final String methodScopeVariable = "This variable's scope is method";

        for (int i = 0; i < 3; i++) {
            String loopScopeVariable = "This variable's scope is loop";
        }

        {
            String blockScopeVariable = "This variable's scope is block";
        }

        System.out.println(classScopeVariable); // Prints "This variable's scope is class"
        final String classScopeVariable = "Shadowed variable"; // This is variable shadowing and it's a bad practice!
        System.out.println(classScopeVariable); // Prints "Shadowed variable"
    }
}
